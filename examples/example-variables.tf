/* -------------------------------------------------------------------------------
This is a small example of different types of variables and how to define them.
------------------------------------------------------------------------------ */
variable "my_string_variable" {
    type = string
    default = "This Is A String"
    #default = "SoIsThis"
}

/* -------------------------------------------------------------------------------
This is a set of strings, think of being basically an array of strings.
------------------------------------------------------------------------------ */
variable "set_of_strings_variable" {
    type = set(string)
    default = {
        "setstring1",
        "setstring2",
        "setstring3"
    }
}

/* -------------------------------------------------------------------------------
This is a map, maps must be in the form of a key,value pair.  In this example a
map has a key (ie. container1) and multiple key/value pairs.
You will most like use these in a "for_each" situation where you want to dynamically
create multiple of the same object or component.
------------------------------------------------------------------------------ */
variable "map_of_strings" {
    type = map(any)
    default = {
        "container1" = {
            memory = "12Gi"
            cpu = "2"
        },
        "container2" = {
            memory = "15Gi"
            cpu = "2"
        }
    }
}

resource "kubernetes_namespace_v1" "create_multiple_namespaces" {
    for_each = var.map_of_strings
    metadata {
        name = each.key
        namespace =         
    }
}


