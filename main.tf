/* -------------------------------------------------------------------------------
This is typically the initial file that terraform reads.
I typically treat this like the config file for terraform.
------------------------------------------------------------------------------ */
terraform {
  required_version = ">= 0.13"

  /* -------------------------------------------------------------------------------
  The backend doesn't like the use of variables so you'll have to populate this.
  What the backend does here is store the terraform state in a kubernetes secret.
  Replace xxxx with your username we will find this later.
  ------------------------------------------------------------------------------ */
  backend "kubernetes" {
    secret_suffix = "xxxx"
    config_path  = ".authconfig"
  }
}

#This is one of the many types of variables.
variable "kube_config" {
    type = string
    default = ".authconfig"
}


#Since we will be using kubernetes, we will drop this here.
provider "kubernetes" {
  config_path = var.kube_config
}

#Since we will also be doing helm charts, we will drop this here also.
provider "helm" {
  kubernetes {
    config_path = var.kube_config
  }
}
